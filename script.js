/*************
 * Global
 * parameters
 *
 *************/
let src = "3*2+2*(4+5)";

let i = 0;
let word = '';
let lexems = new Array();

let currentLexem = 0;
let ast = new Array();



function consume(boolNotNull){
  if(boolNotNull){
    word += src[i];
  }
  i++;
}

function product(type){
  lexems.push({word : word,type : type});
  word = "";
}

/*************
 * Lexer
 *
 *************/

  // num := [0...9]
  function isNum(){
    return("0123456789".includes(src[i]));
  }

  // null := ' '|'\t'|'\n'|\r'
  function isNull(){
    let val = " \t\n\r".includes(src[i]);

    if(val){
      consume(false);
    }

    return(val);
  }

  // symbol := '+'|'-'|'*'|'/'|'('|')'
  function isSymbol(){
    let val = "+-*/()".includes(src[i]);

    if(val){
      consume(true);
      product("SYMBOL");
    }

    return(val);
  }

  // number := num+ {'.'num+}
  function isNumber(){
    if(!isNum(src,i)){
      return false;
    }

    else{
      while(isNum(src,i)){
        consume(true);
      }
      if(src[i]=="."){
        consume(true);
        while(isNum(src,i)){
          consume(true);
        }
      }
      product('NUMBER');
      return true
    }

  }

  function isExpr(){
    if(isSymbol()){return true}
    else if (isNumber()){return true}
    else if (isNull()){return false}
    else{return false}
  }

/*************
 * Parser
 *
 *************/

 function consumeParsing() {
     currentLexem++
 }

function parser(){
  while(lexems[currentLexem].type!="END"){

    const expr = simpleOperation();

    if(expr){
      ast.push(expr);
    }
  }
}


function simpleOperation(){

    const left = nextLevelOperation();

    if (lexems[currentLexem].word == '+') {
        consumeParsing();
        return({ left, type: "+", right: simpleOperation() }) //!!
    }
    else if (lexems[currentLexem].word == '-') {
        consumeParsing();
        return({ left, type: "-", right: simpleOperation() }) //!!
    }
    return left;
}

function nextLevelOperation(){

    const left = numbersOrParenthesis();

    if (lexems[currentLexem].word == '*') {
        consumeParsing();
        return({ left, type: "*", right: nextLevelOperation() }) //!!
    }
    else if (lexems[currentLexem].word == '/') {
        consumeParsing();
        return({ left, type: "/", right: nextLevelOperation() }) //!!
    }

    return left;
}


function numbersOrParenthesis(){
  const curr = lexems[currentLexem];

  if (curr.word == '(') {
      consumeParsing(); //pass after the openning paranthesis

      const left = simpleOperation(); //relaunch the whole process for the expression between parentheses

      if (lexems[currentLexem].word != ')') { //testing the corrct closing of the parenthesis
	console.log("ERROR : Missing a closing parenthesis");
	return({word:"error missing ')'", type:"ERROR"});
	}

      consumeParsing(); //pass after the closing paranthesis
      return left
  }


  if (curr.type == 'NUMBER'){
      consumeParsing();
      return curr
    }
}


/*************
 * Translater from
 * AST to result
 *************/

 function translaterAST(operation) {
     if (operation.left && operation.right) {
         const right = translaterAST(operation.right)
         const left = translaterAST(operation.left)
         if (operation.type == '+') {
             return(left + right)
         }
         if (operation.type == '-') {
             return(left - right)
         }
 	       if (operation.type == '*') {
             return(left * right)
         }
 	       if (operation.type == '/') {
             return(left / right)
         }
       }
      else if (operation.type == 'NUMBER') {
         return(Number(operation.word));
      }
}




 /*************
  * Launch the
  * calculation
  *************/
function main(){
  console.log("Formula :")
  console.log(src);

  while(i<src.length){
    isNull();
    isExpr();
  }

  lexems.push({word : "$",type : "END"})
  console.log("Lexems :");
  console.log(lexems);

  parser();

  console.log("AST :");
  console.log(ast);

  let result = translaterAST(ast[0]);

  console.log("Result :");
  console.log(result);

  return result;
}

main();
